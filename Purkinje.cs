﻿using System.Security.Cryptography.X509Certificates;

namespace EyeBrain.SightSync.Common
{
    /// <summary>
    /// Represents a reflection point on the eye of a patient, also called a P1 Point
    /// </summary>
    public class Purkinje
    {
        /// <summary>
        /// Gets the center.
        /// </summary>
        /// <value>
        /// The center.
        /// </value>
        public PointD Center { get; }

        /// <summary>
        /// Gets the milimeter center.
        /// </summary>
        /// <value>
        /// The milimeter center.
        /// </value>
        public PointD MilimeterCenter { get; set; }

        /// <summary>
        /// Gets or sets the bounds.
        /// </summary>
        /// <value>
        /// The bounds.
        /// </value>
        public Bound Bounds { get; set; }

        /// <summary>
        /// Gets or sets the milimeter bounds.
        /// </summary>
        /// <value>
        /// The milimeter bounds.
        /// </value>
        public Bound MilimeterBounds { get; set; }

        /// <summary>
        /// Gets or sets the fullness.
        /// </summary>
        /// <value>
        /// The fullness.
        /// </value>
        public double Fullness { get; set; }

        /// <summary>
        /// Gets or sets the conversion factor.
        /// </summary>
        /// <value>
        /// The conversion factor.
        /// </value>
        public double ConversionFactor { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Purkinje" /> class.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="bound">The bound.</param>
        /// <param name="conversionFactor">The conversion factor.</param>
        public Purkinje(double x, double y, Bound bound, double conversionFactor)
            : this(new PointD(x, y), bound, conversionFactor)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Purkinje"/> class.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="bound">The bound.</param>
        /// <param name="conversionFactor">The conversion factor.</param>
        public Purkinje(PointD point, Bound bound, double conversionFactor)
        {
            Center = point;
            Bounds = bound;
            CalculateMilimeters(conversionFactor);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Purkinje" /> class.
        /// </summary>
        public Purkinje()
        {
            Center = PointD.Zero;
            Bounds = Bound.Zero;
            MilimeterCenter = PointD.Zero;
            MilimeterBounds = Bound.Zero;
            Fullness = 0;
            ConversionFactor = ConversionFactor;
        }

        /// <summary>
        /// Calculates the milimeters.
        /// </summary>
        /// <param name="conversionFactor">The conversion factor.</param>
        public void CalculateMilimeters(double conversionFactor)
        {
            MilimeterCenter = Center * conversionFactor;
            MilimeterBounds = Bounds * conversionFactor;
            ConversionFactor = conversionFactor;
        }

        /// <summary>
        /// Offsets the specified bound.
        /// </summary>
        /// <param name="bound">The bound.</param>
        public void Offset(Bound bound)
        {
            Center.X += bound.X;
            Center.Y += bound.Y;
            Bounds += bound;
            CalculateMilimeters(ConversionFactor);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "{" + $" {Center}, {Bounds}, {MilimeterCenter}, {MilimeterBounds}, {Fullness} " + "}";
        }
    }
}
