﻿using System.Linq;

namespace EyeBrain.SightSync.Common
{
    /// <summary>
    /// Represents the set of LED lights placed to reflect on the eye surface
    /// </summary>
    public class PurkinjeSet
    {
        /// <summary>
        /// Gets or sets the left top.
        /// </summary>
        /// <value>
        /// The left top.
        /// </value>
        public Purkinje LeftTop
        {
            get => Purkinjes[0];
            set => Purkinjes[0] = value;
        }

        /// <summary>
        /// Gets or sets the rigth top.
        /// </summary>
        /// <value>
        /// The rigth top.
        /// </value>
        public Purkinje RigthTop
        {
            get => Purkinjes[1];
            set => Purkinjes[1] = value;
        }

        /// <summary>
        /// Gets or sets the right bottom.
        /// </summary>
        /// <value>
        /// The right bottom.
        /// </value>
        public Purkinje RightBottom
        {
            get => Purkinjes[2];
            set => Purkinjes[2] = value;
        }

        /// <summary>
        /// Gets or sets the left bottom.
        /// </summary>
        /// <value>
        /// The left bottom.
        /// </value>
        public Purkinje LeftBottom
        {
            get => Purkinjes[3];
            set => Purkinjes[3] = value;
        }

        /// <summary>
        /// Gets the set of purkinjes, LT, RT, RB, LB (Clock wise rotation)
        /// </summary>
        /// <value>
        /// The purkinjes.
        /// </value>
        public Purkinje[] Purkinjes { get; } = new Purkinje[4];

        /// <summary>
        /// Gets the horizontal distance between the 4 P1 Points
        /// </summary>
        /// <value>
        /// The horizontal distance.
        /// </value>
        public double HorizontalDistance => RightBottom != null && RigthTop != null ? (RightBottom.Center.X + RigthTop.Center.X) / 2d - (LeftBottom.Center.X + LeftTop.Center.X) / 2d : 0;

        /// <summary>
        /// Gets the average center.
        /// </summary>
        /// <value>
        /// The average center.
        /// </value>
        public PointD AverageCenter => new PointD(Purkinjes.Average(p => p?.Center.X ?? 0), Purkinjes.Average(p => p?.Center.Y ?? 0));

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="PurkinjeSet"/> is detected.
        /// </summary>
        /// <value>
        ///   <c>true</c> if detected; otherwise, <c>false</c>.
        /// </value>
        public bool Detected { get; set; }

        /// <summary>
        /// Gets the bounds.
        /// </summary>
        /// <value>
        /// The bounds.
        /// </value>
        public Bound Bounds => new Bound(LeftTop.Center.X, LeftTop.Center.Y, RightBottom.Center.X - LeftTop.Center.X, RightBottom.Center.Y - LeftTop.Center.Y);
    }
}
