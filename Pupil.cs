﻿namespace EyeBrain.SightSync.Common
{
    public class Pupil
    {
        /// <summary>
        /// Gets or sets the center.
        /// </summary>
        /// <value>
        /// The center.
        /// </value>
        public PointD Center { get; set; }

        /// <summary>
        /// Gets the milimeter center.
        /// </summary>
        /// <value>
        /// The milimeter center.
        /// </value>
        public PointD MilimeterCenter { get; set;  }

        /// <summary>
        /// Gets or sets the bounds.
        /// </summary>
        /// <value>
        /// The bounds.
        /// </value>
        public Bound Bounds { get; set; }

        /// <summary>
        /// Gets or sets the milimeter bound.
        /// </summary>
        /// <value>
        /// The milimeter bound.
        /// </value>
        public Bound MilimeterBound { get; set; }

        /// <summary>
        /// Gets or sets the fullness.
        /// </summary>
        /// <value>
        /// The fullness.
        /// </value>
        public double Fullness { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Pupil"/> is detected.
        /// </summary>
        /// <value>
        ///   <c>true</c> if detected; otherwise, <c>false</c>.
        /// </value>
        public bool Detected { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Pupil" /> class.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="bound">The bound.</param>
        /// <param name="converionFactor">The converion factor.</param>
        public Pupil(double x, double y, Bound bound, double converionFactor)
                    : this(new PointD(x, y), bound, converionFactor)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Pupil" /> class.
        /// </summary>
        /// <param name="center">The center.</param>
        /// <param name="bound">The bound.</param>
        /// <param name="conversionFactor">The conversion factor.</param>
        public Pupil(PointD center, Bound bound, double conversionFactor)
        {
            Center = center;
            Bounds = bound;
            CalculateMilimeter(conversionFactor);
            Fullness = 0;
            Detected = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Pupil" /> class.
        /// </summary>
        public Pupil()
        {
            Center = PointD.Zero;
            Bounds = Bound.Zero;
            MilimeterCenter = PointD.Zero;
            MilimeterBound = Bound.Zero;
            Fullness = 0;
            Detected = false;
        }

        /// <summary>
        /// Calculates the milimeter.
        /// </summary>
        /// <param name="conversionFactor">The conversion factor.</param>
        public void CalculateMilimeter(double conversionFactor)
        {
            MilimeterCenter = Center * conversionFactor;
            MilimeterBound = Bounds * conversionFactor;
        }
    }
}
