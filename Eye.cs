﻿using System;
using Newtonsoft.Json;

namespace EyeBrain.SightSync.Common
{
    public class Eye
    {
        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>
        /// The location.
        /// </value>
        public EyeLocation Location { get; set; }

        /// <summary>
        /// Gets or sets the pupil.
        /// </summary>
        /// <value>
        /// The pupil.
        /// </value>
        public Pupil Pupil { get; set; } = new Pupil();

        /// <summary>
        /// Gets or sets the purkinje set.
        /// </summary>
        /// <value>
        /// The purkinje set.
        /// </value>
        public PurkinjeSet PurkinjeSet { get; set; } = new PurkinjeSet();

        /// <summary>
        /// Gets or sets the conversion factor.
        /// </summary>
        /// <value>
        /// The conversion factor.
        /// </value>
        public double ConversionFactor { get; set; }

        /// <summary>
        /// Gets or sets the horizonatl rotaion.
        /// </summary>
        /// <value>
        /// The horizonatl rotaion.
        /// </value>
        public double HorizonatlRotaion { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Eye" /> class.
        /// </summary>
        /// <param name="location">The location.</param>
        public Eye(EyeLocation location)
        {
            Location = location;
        }

        /// <summary>
        /// Calculates the rotation.
        /// </summary>
        public void CalculateRotation()
        {
            HorizonatlRotaion = 180 * (Math.Asin(Pupil.Center.X - PurkinjeSet.AverageCenter.X) / 6 / Math.PI);
        }

        /// <summary>
        /// Returns a JSON <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A JSON <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
