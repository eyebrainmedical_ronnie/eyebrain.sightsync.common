﻿using System;
using System.Runtime.InteropServices;

namespace EyeBrain.SightSync.Common
{
    /// <summary>
    /// Loads a DLL using the Kernel32.dll
    /// </summary>
    public static class Native
    {
        //
        // Standard Win32 calls.
        //
        /// <summary>
        /// Loads the library.
        /// </summary>
        /// <param name="dllToLoad">The DLL to load.</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr LoadLibrary(string dllToLoad);

        /// <summary>
        /// Frees the library.
        /// </summary>
        /// <param name="hModule">The h module.</param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool FreeLibrary(IntPtr hModule);
    }
}
