using System;

namespace EyeBrain.SightSync.Common
{
    /// <summary>
    /// Represents the System Configuration Contract
    /// </summary>
    public interface IConfig
    {
        /// <summary>
        /// Gets the device key.
        /// </summary>
        /// <value>
        /// The device key.
        /// </value>
        Guid DeviceKey { get; }

        /// <summary>
        /// Gets the eye configuration location.
        /// </summary>
        /// <value>
        /// The eye configuration location.
        /// </value>
        string EyeConfigLocation { get; }

        /// <summary>
        /// Gets or sets the system configuration file.
        /// </summary>
        /// <value>
        /// The system configuration file.
        /// </value>
        string SystemConfigurationFile { get; }

        /// <summary>
        /// Gets the left eye camera configuration file.
        /// </summary>
        /// <value>
        /// The left eye camera configuration file.
        /// </value>
        string LeftEyeCameraConfigFile { get; }

        /// <summary>
        /// Gets the right eye camera configuration file.
        /// </summary>
        /// <value>
        /// The right eye camera configuration file.
        /// </value>
        string RightEyeCameraConfigFile { get; }

        /// <summary>
        /// Gets the left eye phoropter table file.
        /// </summary>
        /// <value>
        /// The left eye phoropter table file.
        /// </value>
        string LeftEyePhoropterTableFile { get; }

        /// <summary>
        /// Gets the right eye phoropter table file.
        /// </summary>
        /// <value>
        /// The right eye phoropter table file.
        /// </value>
        string RightEyePhoropterTableFile { get; }

        /// <summary>
        /// Gets the pd.
        /// </summary>
        /// <value>
        /// The pd.
        /// </value>
        IPupilaryDistanceConfig Pd { get; }

        /// <summary>
        /// Gets the hardware.
        /// </summary>
        /// <value>
        /// The hardware.
        /// </value>
        IHardwareConfig Hardware { get; }

        /// <summary>
        /// Gets the actuator.
        /// </summary>
        /// <value>
        /// The actuator.
        /// </value>
        IActuatorConfig Actuator { get; }

        /// <summary>
        /// Gets the device.
        /// </summary>
        /// <value>
        /// The device.
        /// </value>
        IDeviceConfig Device { get; }

        /// <summary>
        /// Gets the system.
        /// </summary>
        /// <value>
        /// The system.
        /// </value>
        ISystemConfig System { get; }

        /// <summary>
        /// Gets the screen.
        /// </summary>
        /// <value>
        /// The screen.
        /// </value>
        IScreenConfig Screen { get; }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        void Initialize();
    }
}