namespace EyeBrain.SightSync.Common
{
    public enum EyeLocation
    {
        Left,
        Right,
        Both
    }
}