﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeBrain.SightSync.Common
{
    /// <summary>
    /// 
    /// </summary>
    public static class Checks
    {
        /// <summary>
        /// Determines whether the value is within bounds
        /// </summary>
        /// <param name="min">The minimum.</param>
        /// <param name="max">The maximum.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if [is value within bounsd] [the specified minimum]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValueWithinBounds(int min, int max, int value)
        {
            return value >= min && value <= max;
        }

        /// <summary>
        /// Determines whether [is value within bounds] [the specified minimum].
        /// </summary>
        /// <param name="min">The minimum.</param>
        /// <param name="max">The maximum.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if [is value within bounds] [the specified minimum]; otherwise, <c>false</c>.
        /// </returns>
        public static double Bound(double min, double max, double value)
        {
            var retVal = value;
            retVal = Math.Max(retVal, min);
            retVal = Math.Min(retVal, max);
            return retVal;
        }

        /// <summary>
        /// Bounds the specified minimum.
        /// </summary>
        /// <param name="min">The minimum.</param>
        /// <param name="max">The maximum.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static int Bound(int min, int max, int value)
        {
            var retVal = value;
            retVal = Math.Max(retVal, min);
            retVal = Math.Min(retVal, max);
            return retVal;
        }

        /// <summary>
        /// Calculates the specified percentage.
        /// </summary>
        /// <param name="percentage">The percentage.</param>
        /// <param name="maxvalue">The maxvalue.</param>
        /// <returns></returns>
        public static int CalculatePercenatageOfMax(double percentage, int maxvalue)
        {
            return Convert.ToInt32(Bound(0, 100, percentage) / 100.0 * maxvalue);
        }

        /// <summary>
        /// Calculates the accuracy.
        /// </summary>
        /// <param name="mmTolerance">The mm tolerance.</param>
        /// <param name="mmStrokeLength">Length of the mm stroke.</param>
        /// <param name="maxAdcValue">The maximum adc value.</param>
        /// <returns></returns>
        public static int CalculateAccuracy(double mmTolerance, double mmStrokeLength, int maxAdcValue)
        {
            var retVal = 4;
            if (mmTolerance >= 0 && mmStrokeLength > 0 && maxAdcValue > 0)
            {
                retVal = Convert.ToInt32(mmTolerance / mmStrokeLength * maxAdcValue);
            }

            // Ensure it is bounded to 0,1023 since the microcontroller accepts this value.
            retVal = Bound(0, 1023, retVal);
            return retVal;
        }

        /// <summary>
        /// Is the value within range
        /// </summary>
        /// <param name="deviceValue"></param>
        /// <param name="targetValue"></param>
        /// <param name="accuracySteps"></param>
        /// <param name="deviceLowerBound"></param>
        /// <param name="deviceUpperBound"></param>
        /// <returns></returns>
        public static bool IsActuatorValueWithinAccuracyRange(ushort deviceValue, ushort targetValue, ushort accuracySteps, ushort deviceLowerBound, ushort deviceUpperBound)
        {
            if (deviceValue == ushort.MaxValue)
            {
                // We have a sentinel since we wrap the return value of device to ushort.MaxValue if anything went wrong.
                return false;
            }

            // Convert to int so that it is easy for us to work on.
            var device = Convert.ToInt32(deviceValue);
            var target = Convert.ToInt32(targetValue);
            var steps = Convert.ToInt32(accuracySteps);
            var lowerLimit = Convert.ToInt32(deviceLowerBound);
            var upperLimit = Convert.ToInt32(deviceUpperBound);

            // Find the acceptable bounds. We work in steps so ensure these are bounded.
            var lower = Bound(lowerLimit, upperLimit, target - steps);
            var upper = Bound(lowerLimit, upperLimit, target + steps);

            // Find the answer.
            return device >= lower && device <= upper;
        }
    }
}
