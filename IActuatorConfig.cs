﻿namespace EyeBrain.SightSync.Common
{
    public interface IActuatorConfig
    {
        /// <summary>
        /// Gets or sets the left gradient.
        /// </summary>
        /// <value>
        /// The left gradient.
        /// </value>
        double LeftGradient { get; set; }

        /// <summary>
        /// Gets or sets the left intercept.
        /// </summary>
        /// <value>
        /// The left intercept.
        /// </value>
        double LeftIntercept { get; set; }

        /// <summary>
        /// Gets or sets the right gradient.
        /// </summary>
        /// <value>
        /// The right gradient.
        /// </value>
        double RightGradient { get; set; }

        /// <summary>
        /// Gets or sets the right intercept.
        /// </summary>
        /// <value>
        /// The right intercept.
        /// </value>
        double RightIntercept { get; set; }

        /// <summary>
        /// Gets or sets the actuator speed percentage.
        /// </summary>
        /// <value>
        /// The actuator speed percentage.
        /// </value>
        long ActuatorSpeedPercentage { get; set; }
        /// <summary>
        /// Gets or sets the actuator settle wait.
        /// </summary>
        /// <value>
        /// The actuator settle wait.
        /// </value>
        long ActuatorSettleWait { get; set; }
        /// <summary>
        /// Gets or sets the actuator settle timeout.
        /// </summary>
        /// <value>
        /// The actuator settle timeout.
        /// </value>
        long ActuatorSettleTimeout { get; set; }
        /// <summary>
        /// Gets or sets the actuator total devices.
        /// </summary>
        /// <value>
        /// The actuator total devices.
        /// </value>
        long ActuatorTotalDevices { get; set; }
        /// <summary>
        /// Gets or sets the actuator total adc steps phoropter.
        /// </summary>
        /// <value>
        /// The actuator total adc steps phoropter.
        /// </value>
        long ActuatorTotalAdcStepsPhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator length phoropter.
        /// </summary>
        /// <value>
        /// The actuator length phoropter.
        /// </value>
        long ActuatorLengthPhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator retract limit percentage.
        /// </summary>
        /// <value>
        /// The actuator retract limit percentage.
        /// </value>
        long ActuatorRetractLimitPercentage { get; set; }
        /// <summary>
        /// Gets or sets the actuator extend limit percentage.
        /// </summary>
        /// <value>
        /// The actuator extend limit percentage.
        /// </value>
        long ActuatorExtendLimitPercentage { get; set; }
        /// <summary>
        /// Gets or sets the actuator usable length phoropter.
        /// </summary>
        /// <value>
        /// The actuator usable length phoropter.
        /// </value>
        long ActuatorUsableLengthPhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator usable minimum length phoropter.
        /// </summary>
        /// <value>
        /// The actuator usable minimum length phoropter.
        /// </value>
        long ActuatorUsableMinimumLengthPhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator usable maximum length phoropter.
        /// </summary>
        /// <value>
        /// The actuator usable maximum length phoropter.
        /// </value>
        long ActuatorUsableMaximumLengthPhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator accuracy phoropter.
        /// </summary>
        /// <value>
        /// The actuator accuracy phoropter.
        /// </value>
        double ActuatorAccuracyPhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator accuracy fudge value phoropter.
        /// </summary>
        /// <value>
        /// The actuator accuracy fudge value phoropter.
        /// </value>
        double ActuatorAccuracyFudgeValuePhoropter { get; set; }
        /// <summary>
        /// Gets or sets the actuator last chance settle wait.
        /// </summary>
        /// <value>
        /// The actuator last chance settle wait.
        /// </value>
        long ActuatorLastChanceSettleWait { get; set; }
        /// <summary>
        /// Gets or sets the actuator COM port.
        /// </summary>
        /// <value>
        /// The actuator COM port.
        /// </value>
        string ActuatorComPort { get; set; }
        /// <summary>
        /// Gets or sets the actuator device address phoropter le.
        /// </summary>
        /// <value>
        /// The actuator device address phoropter le.
        /// </value>
        long ActuatorDeviceAddressPhoropterLE { get; set; }
        /// <summary>
        /// Gets or sets the actuator device address phoropter re.
        /// </summary>
        /// <value>
        /// The actuator device address phoropter re.
        /// </value>
        long ActuatorDeviceAddressPhoropterRE { get; set; }
    }
}