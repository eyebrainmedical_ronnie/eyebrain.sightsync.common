﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeBrain.SightSync.Common
{
    public class Bound
    {
        /// <summary>
        /// Gets the left.
        /// </summary>
        /// <value>
        /// The left.
        /// </value>
        public double Left => X;

        /// <summary>
        /// Gets the top.
        /// </summary>
        /// <value>
        /// The top.
        /// </value>
        public double Top => Y;

        /// <summary>
        /// Gets the right.
        /// </summary>
        /// <value>
        /// The right.
        /// </value>
        public double Right => X + Width;

        /// <summary>
        /// Gets the bottom.
        /// </summary>
        /// <value>
        /// The bottom.
        /// </value>
        public double Bottom => Y + Height;
        /// <summary>
        /// Gets or sets the x.
        /// </summary>
        /// <value>
        /// The x.
        /// </value>
        public double X { get; set; }
        /// <summary>
        /// Gets or sets the y.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        public double Y { get; set; }
        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public double Width { get; set; }
        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public double Height { get; set; }

        /// <summary>
        /// Gets the zero.
        /// </summary>
        /// <value>
        /// The zero.
        /// </value>
        public static Bound Zero => new Bound(0, 0, 0, 0);

        /// <summary>
        /// Gets the area.
        /// </summary>
        /// <value>
        /// The area.
        /// </value>
        public double Area => Width * Height;

        /// <summary>
        /// Gets the center.
        /// </summary>
        /// <value>
        /// The center.
        /// </value>
        public PointD Center => new PointD(X + Width / 2d, Y + Height / 2d);

        /// <summary>
        /// Initializes a new instance of the <see cref="Bound"/> class.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public Bound(double x, double y, double width, double height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Bound"/> class.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public Bound(int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Bound" /> class.
        /// </summary>
        /// <param name="rect">The rect.</param>
        public Bound(Rectangle rect)
        {
            X = rect.X;
            Y = rect.Y;
            Width = rect.Width;
            Height = rect.Height;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Bound"/> class.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public Bound(float x, float y, float width, float height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Bound"/> class.
        /// </summary>
        public Bound()
        {
            X = 0;
            Y = 0;
            Width = 0;
            Height = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Bound" /> class.
        /// </summary>
        /// <param name="rect">The rect.</param>
        public Bound(RectangleF rect)
        {
            X = rect.X;
            Y = rect.Y;
            Width = rect.Width;
            Height = rect.Height;
        }

        /// <summary>
        /// returns the divided resulting bounds
        /// </summary>
        /// <param name="bound">The bound.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Bound operator *(Bound bound, double value)
        {
            return new Bound(bound.X * value, bound.Y * value, bound.Width * value, bound.Height * value);
        }

        /// <summary>
        /// Adds the position of the two Bounds together
        /// </summary>
        /// <param name="bound">The bound.</param>
        /// <param name="other">The other.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Bound operator +(Bound bound, Bound other)
        {
            return new Bound(bound.X + other.X, bound.Y + other.Y, bound.Width, bound.Height);
        }

        /// <summary>
        /// Implements the operator -.
        /// </summary>
        /// <param name="bound">The bound.</param>
        /// <param name="other">The other.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Bound operator -(Bound bound, Bound other)
        {
            return new Bound(bound.X - other.X, bound.Y - other.Y, bound.Width, bound.Height);
        }
        /// <summary>
        /// Inflates the specified amount.
        /// </summary>
        /// <param name="amount">The amount.</param>
        public void Inflate(double amount)
        {
            X += amount;
            Y += amount;
            Width += (amount * 2);
            Height += (amount * 2);
        }

        /// <summary>
        /// Deflates the specified amount.
        /// </summary>
        /// <param name="amount">The amount.</param>
        public void Deflate(double amount)
        {
            X -= amount;
            Y -= amount;
            Width -= (amount * 2);
            Height -= (amount * 2);
        }

        /// <summary>
        /// To the rectangle.
        /// </summary>
        /// <returns></returns>
        public RectangleF ToRectangleF()
        {
            return new RectangleF((float)X, (float)Y, (float)Width, (float)Height);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "{" + $" {X}, {Y}, {Width}, {Height}, {Area}, Center:{Center} " + "}";
        }
    }
}
