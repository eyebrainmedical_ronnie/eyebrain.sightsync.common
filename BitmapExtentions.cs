﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace EyeBrain.SightSync.Common
{
    /// <summary>
    /// 
    /// </summary>
    public static class BitmapExtentions
    {
        /// <summary>
        /// To the bitmap image.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static BitmapImage ToBitmapImage(this Bitmap source)
        {
            try
            {
                var bitmapImage = new BitmapImage();
                using (var memory = new MemoryStream())
                {
                    source.Save(memory, ImageFormat.Bmp);
                    memory.Seek(0, SeekOrigin.Begin);
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = memory;
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.EndInit();
                }

                return bitmapImage;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        #region draw routines

        /// <summary>
        /// Draws a 'Bracket' (a rectangle object but only showing the Corners
        /// </summary>
        /// <param name="bmp">The BMP.</param>
        /// <param name="startx">The startx.</param>
        /// <param name="starty">The starty.</param>
        /// <param name="widthx">The widthx.</param>
        /// <param name="widthy">The widthy.</param>
        /// <param name="color">The color.</param>
        /// <param name="size">The size.</param>
        public static void DrawBracket(this System.Drawing.Bitmap bmp, double startx, double starty, double widthx, double widthy, Color color, int size)
        {
            var blackPen = new Pen(color, size);
            var x = (float)startx;
            var y = (float)starty;
            var width = (float)widthx;
            var height = (float)widthy;

            // Draw line to screen.
            using (var graphics = Graphics.FromImage(bmp))
            {
                graphics.DrawLine(blackPen, x, y, x + 30, y);
                graphics.DrawLine(blackPen, x, y, x, y + 30);

                graphics.DrawLine(blackPen, x, y + height, x + 30, y + height);
                graphics.DrawLine(blackPen, x, y + height, x, y - 30 + height);

                graphics.DrawLine(blackPen, x + width, y, x - 30 + width, y);
                graphics.DrawLine(blackPen, x + width, y, x + width, y + 30);

                graphics.DrawLine(blackPen, x + width, y + height, x - 30 + width, y + height);
                graphics.DrawLine(blackPen, x + width, y + height, x + width, y - 30 + height);
            }
        }

        /// <summary>
        /// Draws a purkinje object (Represented by a 'Cross'
        /// </summary>
        /// <param name="bmp">The BMP.</param>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="color">The color.</param>
        /// <param name="size">The size.</param>
        /// <param name="width">The width.</param>
        public static void DrawPurkinje(this Bitmap bmp, float x, float y, Color color, int size, int width = 10)
        {
            var blackPen = new Pen(color, size);
            // Draw line to screen.
            using (var graphics = Graphics.FromImage(bmp))
            {
                graphics.DrawLine(blackPen, x - width, y, x + width, y);
                graphics.DrawLine(blackPen, x, y - width, x, y + width);
            }
        }

        /// <summary>
        /// Draws a line from X,Y to X,Y
        /// </summary>
        /// <param name="bmp">The BMP.</param>
        /// <param name="startx">The startx.</param>
        /// <param name="starty">The starty.</param>
        /// <param name="endx">The endx.</param>
        /// <param name="endy">The endy.</param>
        /// <param name="color">The color.</param>
        /// <param name="size">The size.</param>
        public static void DrawLine(this System.Drawing.Bitmap bmp, double startx, double starty, double endx, double endy, Color color, int size)
        {
            var blackPen = new Pen(color, size);
            // Draw line to screen.
            using (var graphics = Graphics.FromImage(bmp))
            {
                graphics.DrawLine(blackPen, (int)startx, (int)starty, (int)endx, (int)endy);
            }
        }

        /// <summary>
        /// Draws a rectangle at position X,Y for Width and Height
        /// </summary>
        /// <param name="bmp">The BMP.</param>
        /// <param name="startx">The startx.</param>
        /// <param name="starty">The starty.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="color">The color.</param>
        /// <param name="size">The size.</param>
        public static void DrawRectangle(this System.Drawing.Bitmap bmp, double startx, double starty, double width, double height, Color color, int size)
        {
            var blackPen = new Pen(color, size);
            using (var graphics = Graphics.FromImage(bmp))
            {
                graphics.DrawRectangle(blackPen, (int)startx, (int)starty, (int)width, (int)height);
            }
        }
        #endregion
    }
}
