﻿using System;
using System.Drawing;

namespace EyeBrain.SightSync.Common
{
    public class PointD
    {
        /// <summary>
        /// Gets or sets the x.
        /// </summary>
        /// <value>
        /// The x.
        /// </value>
        public double X { get; set; }

        /// <summary>
        /// Gets or sets the y.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        public double Y { get; set; }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="PointD"/> class.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public PointD(double x, double y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Ges the point f.
        /// </summary>
        /// <returns></returns>
        public PointF GePointF()
        {
            return new PointF((float)X, (float)Y);
        }

        /// <summary>
        /// Gets the point.
        /// </summary>
        /// <returns></returns>
        public Point GetPoint()
        {
            return new Point((int)X, (int)Y);
        }

        /// <summary>
        /// Gets the zero.
        /// </summary>
        /// <value>
        /// The zero.
        /// </value>
        public static PointD Zero => new PointD(0, 0);

        /// <summary>
        /// Gets the empty.
        /// </summary>
        /// <value>
        /// The empty.
        /// </value>
        public static PointD Empty => new PointD(double.NaN, double.NaN);

        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static PointD operator *(PointD point, double value)
        {
            return new PointD(point.X * value, point.Y * value);
        }

        /// <summary>
        /// Implements the operator +.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static PointD operator +(PointD point, PointD value)
        {
            return new PointD(point.X + value.X, point.Y + value.Y);
        }

        /// <summary>
        /// Implements the operator +.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static PointD operator +(PointD point, Bound value)
        {
            return new PointD(point.X + value.X, point.Y + value.Y);
        }

        /// <summary>
        /// Determines whether any of the values of the Point is NaN
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is na n]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsNaN()
        {
            return double.IsNaN(X) || double.IsNaN(Y);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "{" + $" {X}, {Y} " + "}";
        }
    }
}
