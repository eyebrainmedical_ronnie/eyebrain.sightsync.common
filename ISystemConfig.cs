﻿namespace EyeBrain.SightSync.Common
{
    /// <summary>
    /// System configuration parameters
    /// </summary>
    public interface ISystemConfig
    {
        /// <summary>
        /// Gets the name of the system.
        /// </summary>
        /// <value>
        /// The name of the system.
        /// </value>
        string SystemName { get; set; }

        /// <summary>
        /// Gets the software version.
        /// </summary>
        /// <value>
        /// The software version.
        /// </value>
        string SoftwareVersion { get; set; }
    }
}