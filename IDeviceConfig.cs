﻿namespace EyeBrain.SightSync.Common
{
    public interface IDeviceConfig
    {
        /// <summary>
        /// Gets or sets the screen distance.
        /// </summary>
        /// <value>
        /// The screen distance.
        /// </value>
        long ScreenDistance { get; set; }

        /// <summary>
        /// Gets or sets the length of the screen.
        /// </summary>
        /// <value>
        /// The length of the screen.
        /// </value>
        long ScreenLength { get; set; }

        /// <summary>
        /// Gets or sets the screen offset.
        /// </summary>
        /// <value>
        /// The screen offset.
        /// </value>
        long ScreenOffset { get; set; }

        /// <summary>
        /// Gets or sets the camera separation.
        /// </summary>
        /// <value>
        /// The camera separation.
        /// </value>
        long CameraSeparation { get; set; }

        /// <summary>
        /// Gets or sets the mirror angle.
        /// </summary>
        /// <value>
        /// The mirror angle.
        /// </value>
        double MirrorAngle { get; set; }

        /// <summary>
        /// Gets or sets the screen angle.
        /// </summary>
        /// <value>
        /// The screen angle.
        /// </value>
        double ScreenAngle { get; set; }

        /// <summary>
        /// Gets or sets the mirror screen distance.
        /// </summary>
        /// <value>
        /// The mirror screen distance.
        /// </value>
        double MirrorScreenDistance { get; set; }
    }
}