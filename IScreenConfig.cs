﻿namespace EyeBrain.SightSync.Common
{
    public interface IScreenConfig
    {
        /// <summary>
        /// Gets or sets the left vertical adjustment.
        /// </summary>
        /// <value>
        /// The left vertical adjustment.
        /// </value>
        double LeftVerticalAdjustment { get; set; }
        /// <summary>
        /// Gets or sets the right vertical adjustment.
        /// </summary>
        /// <value>
        /// The right vertical adjustment.
        /// </value>
        double RightVerticalAdjustment { get; set; }
        /// <summary>
        /// Gets or sets the left horizontal adjustment.
        /// </summary>
        /// <value>
        /// The left horizontal adjustment.
        /// </value>
        double LeftHorizontalAdjustment { get; set; }
        /// <summary>
        /// Gets or sets the right horizontal adjustment.
        /// </summary>
        /// <value>
        /// The right horizontal adjustment.
        /// </value>
        double RightHorizontalAdjustment { get; set; }
    }
}
