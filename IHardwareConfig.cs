﻿namespace EyeBrain.SightSync.Common
{
    public interface IHardwareConfig
    {
        /// <summary>
        /// Gets or sets the stepper motor COM port.
        /// </summary>
        /// <value>
        /// The stepper motor COM port.
        /// </value>
        long StepperMotorCOMPort { get; set; }

        /// <summary>
        /// Gets or sets the left actuator instance.
        /// </summary>
        /// <value>
        /// The left actuator instance.
        /// </value>
        long LeftActuatorInstance { get; set; }

        /// <summary>
        /// Gets or sets the right actuator instance.
        /// </summary>
        /// <value>
        /// The right actuator instance.
        /// </value>
        long RightActuatorInstance { get; set; }

        /// <summary>
        /// Gets or sets the left phoropter address.
        /// </summary>
        /// <value>
        /// The left phoropter address.
        /// </value>
        long LeftPhoropterAddress { get; set; }

        /// <summary>
        /// Gets or sets the right phoropter address.
        /// </summary>
        /// <value>
        /// The right phoropter address.
        /// </value>
        long RightPhoropterAddress { get; set; }

        /// <summary>
        /// Gets or sets the motor step size chin rest.
        /// </summary>
        /// <value>
        /// The motor step size chin rest.
        /// </value>
        long MotorStepSizeChinRest { get; set; }

        /// <summary>
        /// Gets or sets the motor target speed chin rest.
        /// </summary>
        /// <value>
        /// The motor target speed chin rest.
        /// </value>
        long MotorTargetSpeedChinRest { get; set; }

        /// <summary>
        /// Gets or sets the chin motor address.
        /// </summary>
        /// <value>
        /// The chin motor address.
        /// </value>
        long ChinMotorAddress { get; set; }
    }
}
