﻿namespace EyeBrain.SightSync.Common
{
    public interface IPupilaryDistanceConfig
    {
        /// <summary>
        /// Gets or sets the gradient.
        /// </summary>
        /// <value>
        /// The gradient.
        /// </value>
        double Gradient { get; set; }
        /// <summary>
        /// Gets or sets the intercept.
        /// </summary>
        /// <value>
        /// The intercept.
        /// </value>
        double Intercept { get; set; }

        /// <summary>
        /// Gets or sets the left internal marker.
        /// </summary>
        /// <value>
        /// The left internal marker.
        /// </value>
        double LeftInternalMarker { get; set; }

        /// <summary>
        /// Gets or sets the right internal marker.
        /// </summary>
        /// <value>
        /// The right internal marker.
        /// </value>
        double RightInternalMarker { get; set; }
    }
}